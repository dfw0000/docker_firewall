# Class: rees46_ps2
# ===========================
#
# Full description of class rees46_ps2 here.
#
# Parameters
# ----------
#
# Document parameters here.
#
# * `sample parameter`
# Explanation of what this parameter affects and what it defaults to.
# e.g. "Specify one or more upstream ntp servers as an array."
#
# Variables
# ----------
#
# Here you should define a list of variables that this module would require.
#
# * `sample variable`
#  Explanation of how this variable affects the function of this class and if
#  it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#  External Node Classifier as a comma separated list of hostnames." (Note,
#  global variables should be avoided in favor of class parameters as
#  of Puppet 2.6.)
#
# Examples
# --------
#
# @example
#    class { 'rees46_ps2':
#      servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#    }
#
# Authors
# -------
#
# Author Name <author@domain.com>
#
# Copyright
# ---------
#
# Copyright 2017 Your name here, unless otherwise noted.
#
class docker_firewall {

  #Set up firewall
  #Purge unmanaged iptables rules·
  resources { 'firewall':
    purge => false,
  }
  #resources { 'firewallchain':
  #  purge => true,
  #}
  #Make sure order for pre/post is correct
  Firewall {
        before  => Class['docker_firewall::post'],
        require => Class['docker_firewall::pre'],
  }
  #Declare pre and post
  class { ['docker_firewall::pre', 'docker_firewall::post']: }
  # Make sure packages are installed
  class { 'firewall': }
    #Load firewall rules from hiera

  #Check if we have any firewal rules in hiera
  $isfirewall = hiera('firewall', {})
  if $isfirewall {
    $firewall = lookup('firewall', {merge => 'hash'})
  }


  #Evaluate rules
  if $firewall {
    create_resources('firewall_multi', $firewall)
  }

 
}
