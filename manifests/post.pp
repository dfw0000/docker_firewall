class docker_firewall::post {
  firewall { '900 log dropped input chain':
    chain      => 'INPUT',
    jump       => 'LOG',
    log_level  => '6',
    log_prefix => '[IPTABLES INPUT] dropped ',
    proto      => 'all',
    before     => undef,
  }
#  firewall { '901 log dropped forward chain':
#    chain      => 'FORWARD',
#    jump       => 'LOG',
#    log_level  => '6',
#    log_prefix => '[IPTABLES FORWARD] dropped ',
#    proto      => 'all',
#    before     => undef,
#  }
 
  firewall { '999 drop all':
    proto  => 'all',
    action => 'drop',
    before => undef,
  }
}
