class docker_firewall::pre {


    Firewall {
      require => undef,
    }
    # Default firewall rules
    firewall { '000 accept all icmp':
      proto  => 'icmp',
      action => 'accept',
    }->
    firewall { '001 accept all to lo interface':
      proto   => 'all',
      iniface => 'lo',
      action  => 'accept',
    }->
    firewall { '002 accept all to lo interface':
      proto   => 'all',
      iniface => 'weave',
      action  => 'accept',
    }->
    firewall { '003 reject local traffic not on loopback interface':
      iniface     => '! lo',
      proto       => 'all',
      destination => '127.0.0.1/8',
      action      => 'reject',
    }->
    firewall { '004 accept related established rules':
      proto  => 'all',
      state  => ['RELATED', 'ESTABLISHED'],
      action => 'accept',
    }
    #ensure input rules are cleaned out
    firewallchain { 'INPUT:filter:IPv4':
      ensure => present,
      purge  => true,
    }
}
